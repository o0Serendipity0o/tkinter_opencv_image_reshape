# tkinter_opencv_image_reshape

With this code you can modify the perspective of photo  

[<img src="images/reshape.png" width="400"/>](reshape.png)  

Here an exemple :  
- The input image  
[<img src="images/kindle.jpg" width="200"/>](kindle.jpg)

- The image after  
[<img src="images/kindle_reshaped.jpg" width="200"/>](kindle_reshaped.jpg)


## Usage
```bash
python reshape.py
```
