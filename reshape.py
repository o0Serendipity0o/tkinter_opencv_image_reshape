import tkinter as tk
from tkinter import filedialog
from PIL import Image, ImageTk
import cv2
import numpy as np
import os


class Reshape_image:
    def __init__(self, window):
        self.window = window
        self.fileDir = os.path.dirname(os.path.abspath(__file__))
        # Canvas to show the picture
        self.input_canvas = tk.Canvas(self.window, relief='raised')
        self.input_canvas.grid(row=0, column=0, rowspan=6, columnspan=3, padx=5, pady=5)
        self.output_canvas = tk.Canvas(self.window, width=500, height=500, relief='raised')
        self.output_canvas.grid(row=0, column=7, rowspan=3, columnspan=3, padx=5, pady=5)
        # Menu bar to open, save an image and quit
        self.menubar = tk.Menu(self.window)
        self.window.config(menu=self.menubar)
        self.filemenu = tk.Menu(self.menubar, tearoff=0)
        self.menubar.add_cascade(label="File", menu=self.filemenu)
        self.filemenu.add_command(label="open", command=self.open_image)
        self.filemenu.add_command(label="Save", command=self.save)
        self.filemenu.add_separator()
        self.filemenu.add_command(label="Quit", command=self.quit)
        # Frame with the entry boxes and button
        self.parameter_frame = tk.Frame(self.window)
        self.parameter_frame.grid(row=0, column=4)
        self.parameter_label = tk.Label(self.parameter_frame, text='Parameters')
        self.parameter_label.grid(row=0, column=1, columnspan=2)
        self.coordinate_label = tk.Label(self.parameter_frame)
        self.coordinate_label.grid(row=5, column=2, columnspan=2)
        # buttons
        self.reshape_button = tk.Button(self.parameter_frame, text='reshape', command=self.reshape)
        self.reshape_button.grid(row=7, column=1, padx=5, pady=5)
        self.lines_button = tk.Button(self.parameter_frame, text='draw ROI', command=self.draw)
        self.lines_button.grid(row=7, column=0)
        self.save_button = tk.Button(self.parameter_frame, text='save image', command=self.save)
        self.save_button.grid(row=7, column=2)
        # labels for the parameters
        self.top_left_label = tk.Label(self.parameter_frame, text='top left:')
        self.top_left_label.grid(row=1, column=0)
        self.top_right_label = tk.Label(self.parameter_frame, text='top right:')
        self.top_right_label.grid(row=1, column=2)
        self.down_left_label = tk.Label(self.parameter_frame, text='down left:')
        self.down_left_label.grid(row=3, column=0)
        self.down_right_label = tk.Label(self.parameter_frame, text='down right:')
        self.down_right_label.grid(row=3, column=2)
        # entry for the parameters x,y
        self.ax_entry = tk.Entry(self.parameter_frame, width=7)
        self.ax_entry.grid(row=2, column=0, pady=5)
        self.ay_entry = tk.Entry(self.parameter_frame, width=7)
        self.ay_entry.grid(row=2, column=1, pady=5)
        self.bx_entry = tk.Entry(self.parameter_frame, width=7)
        self.bx_entry.grid(row=2, column=2, pady=5)
        self.by_entry = tk.Entry(self.parameter_frame, width=7)
        self.by_entry.grid(row=2, column=3, pady=5)
        self.cx_entry = tk.Entry(self.parameter_frame, width=7)
        self.cx_entry.grid(row=4, column=0, pady=5)
        self.cy_entry = tk.Entry(self.parameter_frame, width=7)
        self.cy_entry.grid(row=4, column=1, pady=5)
        self.dx_entry = tk.Entry(self.parameter_frame, width=7)
        self.dx_entry.grid(row=4, column=2, pady=5)
        self.dy_entry = tk.Entry(self.parameter_frame, width=7)
        self.dy_entry.grid(row=4, column=3, pady=5)
        self.input_canvas.bind('<B1-Motion>', self.coordinates)
        global is_drawn
        is_drawn = True

    def open_image(self):
        filename = filedialog.askopenfilename(initialdir=self.fileDir)
        photo = cv2.imread(filename)
        self.rows, self.cols, self.ch = photo.shape
        self.input_width = int(self.cols / 5)
        self.input_height = int(self.rows / 5)
        self.input_half_width = int(self.input_width / 2)
        self.input_half_height = int(self.input_height / 2)
        self.photo = cv2.cvtColor(photo, cv2.COLOR_RGB2BGR)
        self.input_image = Image.fromarray(self.photo)
        image = self.input_image.resize((self.input_width, self.input_height), Image.ANTIALIAS)
        self.render = ImageTk.PhotoImage(image)
        self.input_canvas.config(width=self.input_width, height=self.input_height)
        self.input_canvas.create_image(self.input_half_width, self.input_half_height, image=self.render)
        self.input_canvas.image = self.render

    def parameters(self):
        ax = int(self.ax_entry.get())
        ay = int(self.ay_entry.get())
        bx = int(self.bx_entry.get())
        by = int(self.by_entry.get())
        cx = int(self.cx_entry.get())
        cy = int(self.cy_entry.get())
        dx = int(self.dx_entry.get())
        dy = int(self.dy_entry.get())
        return ax, ay, bx, by, cx, cy, dx, dy

    def reshape(self):
        ax, ay, bx, by, cx, cy, dx, dy = self.parameters()
        ax = ax * 5
        ay = ay * 5
        bx = bx * 5
        by = by * 5
        cx = cx * 5
        cy = cy * 5
        dx = dx * 5
        dy = dy * 5

        widthA = np.sqrt(((dx - cx) ** 2) + ((dy - cy) ** 2))
        widthB = np.sqrt(((bx - ax) ** 2) + ((by - ay) ** 2))
        maxWidth = max(int(widthA), int(widthB))

        heightA = np.sqrt(((bx - dx) ** 2) + ((by - dy) ** 2))
        heightB = np.sqrt(((ax - cx) ** 2) + ((ay - cy) ** 2))
        maxHeight = max(int(heightA), int(heightB))

        rect = np.array([[ax, ay], [bx, by], [cx, cy], [dx, dy]], dtype="float32")
        dst = np.array([[0, 0], [maxWidth - 1, 0], [0, maxHeight - 1],
                        [maxWidth - 1, maxHeight - 1]], dtype="float32")
        M = cv2.getPerspectiveTransform(rect, dst)
        warped = cv2.warpPerspective(self.photo, M, (maxWidth, maxHeight))

        self.reshaped = Image.fromarray(warped)
        width = int(maxWidth / 5)
        height = int(maxHeight / 5)
        img = self.reshaped.resize((width, height), Image.ANTIALIAS)
        rdr = ImageTk.PhotoImage(img)
        self.output_canvas.config(width=width, height=height)
        self.output_canvas.create_image((int(width / 2)), (int(height / 2)), image=rdr)
        self.output_canvas.image = rdr

    def coordinates(self, e):
        self.coordinate_label.config(text='coordinates: x:' + str(e.x) + ' y:' + str(e.y))

    def lines(self):
        ax, ay, bx, by, cx, cy, dx, dy = self.parameters()
        line_1 = self.input_canvas.create_line(ax, ay, bx, by, width=2, fill='red')
        line_2 = self.input_canvas.create_line(cx, cy, ax, ay, width=2, fill='red')
        line_3 = self.input_canvas.create_line(cx, cy, dx, dy, width=2, fill='red')
        line_4 = self.input_canvas.create_line(dx, dy, bx, by, width=2, fill='red')
        return line_1, line_2, line_3, line_4

    def delete_lines(self, line_1, line_2, line_3, line_4):
        self.input_canvas.delete(line_1)
        self.input_canvas.delete(line_2)
        self.input_canvas.delete(line_3)
        self.input_canvas.delete(line_4)

    def draw(self):
        global is_drawn
        if is_drawn:
            is_drawn = False
            self.line_1, self.line_2, self.line_3, self.line_4 = self.lines()
            self.lines_button.config(text='delete ROI')
        else:
            is_drawn = True
            self.delete_lines(self.line_1, self.line_2, self.line_3, self.line_4)
            self.lines_button.config(text='draw ROI')

    def save(self):
        files_types = [("JPEG", ('*.jpg', '*.jpeg', '*.jpe', '*.jfif')), ('PNG', '*.png')]
        fname = filedialog.asksaveasfilename(title='Save file', filetypes=files_types)
        self.reshaped.save(str(fname) + '.png', 'PNG')

    def quit(self):
        self.window.destroy()


def main():
    root = tk.Tk()
    app = Reshape_image(root)
    root.mainloop()


if __name__ == '__main__':
    main()
